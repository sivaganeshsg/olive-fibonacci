﻿using System.Collections.Generic;

namespace Web.Services
{
    public class FibonacciService : IFibonacciService
    {
        public IEnumerable<int> GenerateFibonacciSequence(int numbersToGenerate)
        {

            List<int> fibList = new List<int>();

            if(numbersToGenerate > 0)
            {
                // First Seeder Value - F(0) = 0
                fibList.Add(0);
                if (numbersToGenerate > 1)
                {
                    // Second Seeder Value - F(1) = 1
                    fibList.Add(1);

                    // Generate Fibonacci list based on the seeder and 
                    // using the formula F(n) = F(n-1) + F(n-2)
                    for (int i = 2; i < numbersToGenerate; i++)
                    {
                        int prev = fibList[i - 1];
                        int prev_prev = fibList[i - 2];
                        fibList.Add(prev + prev_prev);
                    }
                }
            }
            
            return fibList;

        }
    }
}