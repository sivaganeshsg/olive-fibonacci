﻿using System.Web.Mvc;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    public class FibonacciController : Controller
    {
        private readonly IFibonacciService _service;

        public FibonacciController(IFibonacciService service)
        {
            _service = service;
        }
        
        public ActionResult Index(int number=0)
        {
            var viewModel = new FibonacciViewModel
            {
                Results = _service.GenerateFibonacciSequence(number),
                NumberRequested = number
            };

            return View(viewModel);
        }
    }
}