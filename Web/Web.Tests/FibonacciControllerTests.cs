﻿using System.Web.Mvc;
using NUnit.Framework;
using Web.Controllers;
using Web.Models;
using Moq;
using Web.Services;
using System.Collections.Generic;

namespace Web.Tests
{
    [TestFixture]
    public class FibonacciControllerTests
    {
        [Test]
        public void ItShouldAccceptParameterAndAddToViewModel()
        {
            //Arrange
            var result = GetViewResultFromController();

            //Act
            var viewModel = result.Model as FibonacciViewModel;

            //Assert
            Assert.That(viewModel.NumberRequested, Is.EqualTo(0));
        }

        private static ViewResult GetViewResultFromController()
        {
            Mock<IFibonacciService> _FibonacciServiceMock = new Mock<IFibonacciService>();
            _FibonacciServiceMock.Setup(x => x.GenerateFibonacciSequence(It.IsAny<int>())).Returns(new List<int>());
            
            var controller = new FibonacciController(_FibonacciServiceMock.Object);

            var result = controller.Index() as ViewResult;
            return result;
        }

        [Test]
        public void ItShouldReturnFibonacciViewModel()
        {
            var result = GetViewResultFromController();

            Assert.That(result.Model, Is.TypeOf<FibonacciViewModel>());
        }
    }
}