﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Web.Services;

namespace Web.Tests
{
    [TestFixture]
    public class FibonacciServiceTests
    {
        // Check for Wrong Output
        [Test]
        public void GenerateFibonacciSequence_1_Request_2_Count_Response()
        {
            //arrange
            IFibonacciService fibonacciService = new FibonacciService();
            //act
            IEnumerable<int> fibResult = fibonacciService.GenerateFibonacciSequence(1);
            //assert
            Assert.AreNotEqual(fibResult.Count(), 2);
        }

        // Check for Correct Output
        [Test]
        public void GenerateFibonacciSequence_1_Request_1_Count_Response()
        {
            IFibonacciService fibonacciService = new FibonacciService();
            IEnumerable<int> fibResult = fibonacciService.GenerateFibonacciSequence(1);
            Assert.AreEqual(fibResult.Count(), 1);
        }

        // Check for Wrong Output
        [Test]
        public void GenerateFibonacciSequence_4_Request_Check_For_The_Wrong_Values_In_The_Response()
        {
            IFibonacciService fibonacciService = new FibonacciService();
            IEnumerable<int> fibResult = fibonacciService.GenerateFibonacciSequence(4);
            List<int> expectedFibResult = new List<int>(new int[] { 0, 1, 1, 2, 3 });
            CollectionAssert.AreNotEqual(expectedFibResult, fibResult);
        }

        // Check for Correct Output
        [Test]
        public void GenerateFibonacciSequence_4_Request_Check_For_The_Correct_Values_In_The_Response()
        {
            IFibonacciService fibonacciService = new FibonacciService();
            IEnumerable<int> fibResult = fibonacciService.GenerateFibonacciSequence(4);
            List<int> expectedFibResult = new List<int>(new int[] { 0, 1, 1, 2 });
            CollectionAssert.AreEqual(expectedFibResult, fibResult);
        }



    }
}